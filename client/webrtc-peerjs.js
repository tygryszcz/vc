if (Meteor.isClient) {

  Template.hello.events({
    "click #makeCall": function () {
      var user = this;
      console.log(this);
      var outgoingCall = peer.call(user.profile.peerId, window.localStream);
      window.currentCall = outgoingCall;
      outgoingCall.on('stream', function (remoteStream) {
        window.remoteStream = remoteStream;
        var video = document.getElementById("theirVideo")
        video.src = URL.createObjectURL(remoteStream);
      });
    },

    "click #endCall": function () {
      window.currentCall.close();
    }
  });

  Template.hello.helpers({
    users: function () {
      //var userIds = Presences.find().map(function(presence) {return presence.userId;});
      // exclude the currentUser
      //return Meteor.users.find({_id: {$in: userIds, $ne: Meteor.userId()}});
      return Meteor.users.find();
    }
  });

  Template.hello.onCreated(function () {
      Meteor.subscribe("allusers");

      // Create a Peer instance
      window.peer = new Peer({
      key: 'lc3ccpdnv4klz0k9',
      debug: 3,
      config: {'iceServers': [
        { url: 'stun:stun.l.google.com:19302' },
        { url: 'stun:stun1.l.google.com:19302' },
      ]}
    });

    navigator.getUserMedia = ( navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia );

    // get audio/video
    navigator.getUserMedia({audio:true, video: true}, function (stream) {
      //display video
      var video = document.getElementById("myVideo");
      video.src = URL.createObjectURL(stream);
      window.localStream = stream;
    },
    function (error) { console.log(error); }
    );



    // Handle event: upon opening our connection to the PeerJS server
    peer.on('open', function () {
      //$('#myPeerId').text(peer.id);
      // update the current user's profile
      Meteor.users.update({_id: Meteor.userId()}, {
        $set: {
          profile: { peerId: peer.id}
        }
      });
    });

    // Handle event: remote peer receives a call
    peer.on('call', function (incomingCall) {
      window.currentCall = incomingCall;
      incomingCall.answer(window.localStream);
      incomingCall.on('stream', function (remoteStream) {
        window.remoteStream = remoteStream;
        var video = document.getElementById("theirVideo")
        video.src = URL.createObjectURL(remoteStream);
      });
    });
  });
}

// http://www.mhurwi.com/a-basic-webrtc-tutorial-using-meteor-peerjs/